# Requirements setup on Ubuntu

Prior to playSMS installation the server must be prepared by installing playSMS required applications.

In this tutorial the preparation is done for Ubuntu with LAMP stack.

1.  Upgrade

    ```
    sudo apt-get update
    sudo apt-get upgrade -y
    ```

2.  Install LAMP stack and some PHP extensions

    ```
    sudo apt-get install -y unzip apache2 mysql-server php libapache2-mod-php php-zip php-mysql php-mbstring php-xml php-gd php-imap php-curl
    ```

3.  Restart `apache2`

    ```
    sudo systemctl restart apache2
    ```

4.  Browser `http://localhost` and see if the web server is serving

5.  Download playSMS package from `https://sourceforge.net/projects/playsms/files/playsms/` and save it or upload it to the server
